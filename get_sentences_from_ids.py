import pandas as pd
import glob
import json
import re
import sys
import getopt


def usage():
    print(f"Usage : {sys.argv[0]} -d <path_to_clister_directory> -c <path_to_cas_directory>")

# print(path_clister)
# print(path_cas)

"""
Writes a object to a json file
...
Parameters
----------
path : str
    the output path to write on
dic : str
    the object to save (must be json serializable)
"""
def save_json(path, dic):
    with open(path, "w", encoding="utf-8") as fout:
        fout.write(json.dumps(dic, indent=2, ensure_ascii=False))

"""
Converts the id of the sentence into the real sentence
...
Parameters
----------
sentence_id : str
    the sentence id to convert
chemin_corpus : str
    the path to the cas corpus (must contain all text files)
"""
def sentence_from_id(sentence_id, path_cas):
    fic, ind, long = sentence_id.split("_")
    with open("%s/%s-cas.txt" % (path_cas, fic), "r", encoding="utf-8") as fin:
        deb = int(ind)
        end = deb + int(long)
        return fin.read()[deb:end]
        
"""
Build a dictionary sentence_id : sentence
...
Parameters
----------
chemin_paires : str
    the path to the CLISTER corpus files
chemin_corpus : str
    the path to the cas corpus (must contain all text files)
"""
def recup_phrases(path_clister, path_cas):
    df = pd.read_csv(path_clister, sep="\t")
    paires = zip(df["id_1"], df["id_2"])

    dic_phrases = {}
    
    for p1, p2 in paires:
        m = re.search("(.+\d+.*)_\d+", p1)
        id_1_base = m.group(1)
        m = re.search("(.+\d+.*)_\d+", p2)
        id_2_base = m.group(1)
        dic_phrases[id_1_base] = sentence_from_id(p1, path_cas)
        dic_phrases[id_2_base] = sentence_from_id(p2, path_cas)

    return dic_phrases

try:
    opts, args = getopt.getopt(sys.argv[1:], "hd:c:", ["help", "directory_clister=", "directory_cas="])
except getopt.GetoptError:
    usage()
    sys.exit(2)

for opt, arg in opts:
    if opt in ("-h", "--help"):
        usage()
        sys.exit()
    elif opt in ("-d", "--directory_clister"):
        path_clister = arg
    elif opt in ("-c", "--directory_cas"):
        path_cas = arg

try:
    train = "%s/train.csv" % path_clister
    test = "%s/test.csv" % path_clister
except NameError:
    print("Path to clister corpus must be specified using -d argument")
    usage()
    sys.exit(2)    

try:
    train_id_to_sentence = recup_phrases(train, path_cas)
    test_id_to_sentence = recup_phrases(test, path_cas)
except NameError:
    print("Path to cas corpus must be specified using -c argument")
    usage()
    sys.exit(2)    


save_json("id_to_sentence_train.json", train_id_to_sentence)
save_json("id_to_sentence_test.json", test_id_to_sentence)
