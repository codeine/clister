# CLISTER - A Corpus for Semantic Textual Similarity in French Clinical Narratives

CLISTER is a Semantic Textual Similarity corpus in French constructed based on the CAS corpus (https://deft.limsi.fr/2020/), a French corpus with clinical cases. 

The paper corresponding to the corpus can be found [here](https://hal.inria.fr/hal-03680563).

The CLISTER corpus containes 1,000 sentence pairs annotated with a similarity score on a scale of 0 to 5, 0 being the minimum similarity score and 5 being the maximum. 

### How to use

The corpus is separated in training and testing sets (files __train.tsv__ and __test.tsv__). Those files contain pairs of IDs, corresponding to the sentences in the CAS corpus, and the annotations. 

Once the CAS corpus is acquired, the script (```get_sentences_from_ids.py```) allows to build a json dictionary associating the IDs of the annotation file with the corresponding sentence in the corpus. 

The script can be used this way: 
```
python get_sentences_from_ids.py -d path_to_clister -c path_to_cas
```

With ```path_to_clister``` pointing towards the directory with both __train.csv__ and __test.csv__, and ```path_to_cas``` pointing towards a directory with all the text files of the CAS corpus. 

The scripts generates two files containing json dictionaries. It is then possible to use those dictionaries to associate the sentences to the IDs in the pairs of the CLISTER corpus. 

### Citing

```
@InProceedings{hiebel-EtAl:2022:LREC,
  author    = {Hiebel, Nicolas  and  Ferret, Olivier  and  Fort, Karën  and  Névéol, Aurélie},
  title     = {CLISTER : A Corpus for Semantic Textual Similarity in French Clinical Narratives},
  booktitle      = {Proceedings of the Language Resources and Evaluation Conference},
  month          = {June},
  year           = {2022},
  address        = {Marseille, France},
  publisher      = {European Language Resources Association},
  pages     = {4306--4315},
  abstract  = {Modern Natural Language Processing relies on the availability of annotated corpora for training and evaluating models. Such resources are scarce, especially for specialized domains in languages other than English. In particular, there are very few resources for semantic similarity in the clinical domain in French. This can be useful for many biomedical natural language processing applications, including text generation. We introduce a definition of similarity that is guided by clinical facts and apply it to the development of a new French corpus of 1,000 sentence pairs manually annotated according to similarity scores. This new sentence similarity corpus is made freely available to the community. We further evaluate the corpus through experiments of automatic similarity measurement. We show that a model of sentence embeddings can capture similarity with state-of-the-art performance on the DEFT STS shared task evaluation data set (Spearman=0.8343). We also show that the CLISTER corpus is complementary to DEFT STS.},
  url       = {https://aclanthology.org/2022.lrec-1.459}
}
```
